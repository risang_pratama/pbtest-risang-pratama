# Meet Pricebook
Pricebook is a growth stage start-up, helping users to discover which shop offers the best price today in real-time.

## The Story
The story below is fictional, created only for this test's purpose.

In our business process, one important goal of the platform is to be able to correctly display item price taken from many sources both online and offline shops.

Problem arise when every shop has their own style to name their product. Let's take `Samsung Galaxy S8` for example. These product names actually belongs to same item:

- Samsung Galaxy S8 - Midnight Black
- Galaxy S8 Midnight Black
- Samsung Galaxy S8 Midnight Black (NEW!!!)
- Samsung Galaxy S8 - Black [Merchant]

On the other hand, we also need to distinguish following items, as they are not belongs to the actual smartphone:

- Samsung Clear View Standing Cover for Samsung Galaxy S8
- Rearth Samsung Galaxy S8 Case Ringke Air Prism Thin TPU
- Samsung Starter Kit Basic for Samsung Galaxy S8

## Task
You need to write a simple REST web service application, where the user will be allowed to submit a product name. Once product name submitted, the application should be able to determine which product actually is.

Product names that we will submit to the application for this test:

- Samsung Galaxy S8 - Midnight Black
- Galaxy S8 Midnight Black
- Samsung Galaxy S8 Midnight Black (NEW!!!)
- Samsung Galaxy S8 - Black [Merchant]
- Apple iPhone 7 Plus 256GB - Rose Gold
- iPhone 7plus 256 GB Rose Gold (September Promo)
- Spigen iPhone 7 Case Slim Armor Series
- Samsung Starter Kit Basic for Samsung Galaxy S8

And correct product name the application should returned can be one of the following:

- Samsung Galaxy S8 Midnight Black
- Apple iPhone 7 Plus 256GB Rose Gold
- Spigen iPhone 7 Case Slim Armor Series
- Samsung Starter Kit Basic for Samsung Galaxy S8

### Example

| We Submit This Product Name | Application Should Response |
| --------------------------- | --------------------------- |
| Samsung Galaxy S8 - Black [Merchant] | Samsung Galaxy S8 Midnight Black |
| iPhone 7plus 256 GB Rose Gold (September Promo) | Apple iPhone 7 Plus 256GB Rose Gold |
| Samsung Starter Kit Basic for Samsung Galaxy S8 | Samsung Starter Kit Basic for Samsung Galaxy S8 |

## Requirements

- The service should be able to guess the correct product name, and the probability of correctness in percentage.
- You are **NOT allowed** to use any full-stack web framework (e.g. Laravel, Symfony, Zend Framework, etc.). Any microframework, or library, or component (even if they're part of a full-stack framework) **are okay to use** (e.g. Lumen, Silex, Slim, Symfony Components, Zend Components), as long as it is in PHP and can be installed with Composer. We will be particularly interested in the way how the application make a smart guess. Be careful if you are using library for this specific problem, because the library code will be considered.
- You are free to use any storage type you prefer. Please provide instructions on how to create a schema if necessary.
- Git knowledge is required. Workflow is also evaluated, so having history is desirable. It is recommended that you push your changes as frequently as possible to the repository, and not only at the very end.
- You can deliver your running application using any tool you prefer, as long as you provide clear instructions on how to run it.
- Use any tool you think make sense to ensure that good quality code is delivered.

## Evaluation Process

- We will read the code first. The criteria are ordered by importance (ranked highest to lowest):
    - Code quality
    - Code quality
    - Code quality, seriously.
    - Maintainability
    - Code design. Procedural = automatic fail.
    - Data storage choice
    - Task understanding
    - Task completeness
- We will evaluate the workflow if Git history is available.
- We will run the application and send HTTP requests.
- **We will scramble the product name**, for example: `(September Promo) iPhone 7 Plus Rose Gold 256GB`

## Optional

Just describe how you would implement it.

- **Performance Consideration** This is the very last evaluation point. Keep in mind that this is not the main goal of this application, since the users are our own employees, even if it takes 10 minutes to load the page, the business can still be successful.
- **Describe the flaws of technologies you are using**. For example, if you decided to use Lumen, then you should say which parts of the framework are bad and reasons why you consider them bad.
- This test is also a **continuous improvement process**. Suggest how you would improve it.

Good luck!

# The Answer
First of all i want to say thank you for the skill test.  Here i will explain how i did the skill test.

## Technologies
-  **Lumen Microframework** (https://lumen.laravel.com/) Lumen is fast microframework for created REST API.
-  **Apache Solr** (https://lucene.apache.org/solr/) Apache Solr is open source search platform for indexing and searching. I use Solr as data storage.
By default, Lumen does not support Solr, than i create a class library for Solr.  So i can use Solr with Lumen.

## How to Run

### Prerequisites
What things you need to run this api
1.  PHP >= 7.2 (I use PHP 7.4)
2.  Composer
3.  Docker & Docker Compose

### Usage
Make sure you have installed the prerequisites.  Open terminal, enter directory root api. Then:

**Step 1 : Install Packages Depedencies**
```
composer install
```

**Step 2 : Run docker-compose.yml**
```
docker-compose up -d
``` 

**Step 3 : Run Command for Create Collection**
```
php artisan solr:create-collection products
``` 

**Step 4 : Run Command for Reindex Product**
```
php artisan solr:reindex products
``` 

**Step 5 : Serve the API**
To serve the api, we use the built-in PHP development server.
```
php -S localhost:8000 -t public
```

To use API you can access the url http://localhost:8000/product and send query param q with the product name keyword value.
```
http://localhost:8000/product?q=Samsung Galaxy S8 - Black [Merchant]
```