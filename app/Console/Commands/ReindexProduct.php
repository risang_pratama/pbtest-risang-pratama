<?php


namespace App\Console\Commands;


use App\Libraries\Solr;
use Exception;
use Illuminate\Console\Command;

class ReindexProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr:reindex {collection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex data product to Solr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $collection = $this->argument('collection');
        $solr = new Solr($collection);

        $products = [
            [
                "id" => "samsung-galaxy-s8-midnight-black",
                "name" => "Samsung Galaxy S8 Midnight Black"
            ],
            [
                "id" => "apple-iphone-7-plus-256gb-rose-gold",
                "name" => "Apple iPhone 7 Plus 256GB Rose Gold"
            ],
            [
                "id" => "spigen-iphone-7-case-slim-armor-series",
                "name" => "Spigen iPhone 7 Case Slim Armor Series"
            ],
            [
                "id" => "samsung-starter-kit-basic-for-samsung-galaxy-s8",
                "name" => "Samsung Starter Kit Basic for Samsung Galaxy S8"
            ]
        ];

        try {
            $solr->add($products);
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return ;
        }

        $this->info('Success Reindex Product');
    }
}