<?php


namespace App\Console\Commands;


use App\Libraries\Solr;
use Exception;
use Illuminate\Console\Command;

class CreateCollection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'solr:create-collection {collection}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new collection and schema on Solr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $collection = $this->argument('collection');
        $solr = new Solr($collection);

        // Create Collection
        try {
            $solr->createCollection();
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return ;
        }
        $this->info('Success Create Collection');

        // Create Schema
        $field = [
            'name' => 'name',
            'type' => 'text_general',
            'indexed' => true,
            'stored' => true,
            'multiValued' => false
        ];

        try {
            if(!empty($solr->getField($field['name']))) {
                $solr->deleteField($field['name']);
            }
            $solr->createField($field);
            $solr->createCopyField('name', '_text_');
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return ;
        }
        $this->info('Success Create Schema');
    }
}