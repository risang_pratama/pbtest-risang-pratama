<?php


namespace App\Libraries;


use Exception;
use Illuminate\Support\Facades\Http;

class Solr
{
    private $baseUrl;
    private $collection;

    function __construct($collection)
    {
        $this->baseUrl = config('solr.base_url');
        $this->collection = $collection;
    }

    function createCollection()
    {
        $url = $this->baseUrl."admin/collections";
        $method = "get";
        $params = [
            "action" => "CREATE",
            "name" => $this->collection,
            "numShards" => 1,
            "replicationFactor" => 1,
            "wt" => "json"
        ];
        $response = $this->processRequest($url, $params, $method);
        if(!$response->ok()) {
            throw new Exception('Failed to create collection');
        }
    }

    function createField($field)
    {
        $url = $this->baseUrl.$this->collection."/schema";
        $method = "post";
        $headers = [
            'Content-type' => 'application/json'
        ];
        $data = [
            "add-field" => $field
        ];
        $response = $this->processRequest($url, $data, $method, $headers);
        if(!$response->ok()) {
            throw new Exception('Failed to create field: '.var_dump($response->json()));
        }
    }

    function getField($field_name)
    {
        $url = $this->baseUrl.$this->collection."/schema/fields/".$field_name."?wt=json";
        $method = "get";
        $params = [];
        $response = $this->processRequest($url, $params, $method);
        if($response->ok()) {
            $result = $response->json();
            return $result['field'];
        }
        return null;
    }

    function deleteField($field_name)
    {
        $url = $this->baseUrl.$this->collection."/schema";
        $method = "post";
        $headers = [
            'Content-type' => 'application/json'
        ];
        $data = [
            "delete-field" => [
                "name" => $field_name
            ]
        ];
        $response = $this->processRequest($url, $data, $method, $headers);
        if(!$response->ok()) {
            throw new Exception('Failed to delete field: '.var_dump($response->json()));
        }
    }

    function createCopyField($source_fields, $dest_fields)
    {
        $url = $this->baseUrl.$this->collection."/schema";
        $method = "post";
        $headers = [
            'Content-type' => 'application/json'
        ];
        $data = [
            "add-copy-field" => [
                "source" => $source_fields,
                "dest" => $dest_fields
            ]
        ];
        $response = $this->processRequest($url, $data, $method, $headers);
        if(!$response->ok()) {
            throw new Exception('Failed to create copy field');
        }
    }

    function add($data)
    {
        $url = $this->baseUrl.$this->collection."/update?commit=true";
        $method = "post";
        $headers = [
            'Content-type' => 'application/json'
        ];
        $response = $this->processRequest($url, $data, $method, $headers);
        if(!$response->ok()) {
            throw new Exception('Failed to add data');
        }
    }

    function delete($id)
    {
        $url = $this->baseUrl.$this->collection."/update?commit=true";
        $method = "post";
        $headers = [
            'Content-type' => 'application/json'
        ];
        $data = [
            "delete" => [
                'id' => $id
            ]
        ];
        $response = $this->processRequest($url, $data, $method, $headers);
        if(!$response->ok()) {
            throw new Exception('Failed to delete data');
        }
    }

    function search($q, $start=0, $rows=10)
    {
        $url = $this->baseUrl.$this->collection."/query";
        $method = "get";
        $params = [
            "q" => $q,
            "start" => $start,
            "rows" => $rows
        ];
        return $this->processRequest($url, $params, $method);
    }

    function processRequest($url, $data, $method, $headers=[])
    {
        $http_request = Http::withHeaders($headers);
        if ($method == 'post') {
            $response = $http_request->post($url, $data);
        } else {
            $response = $http_request->get($url, $data);
        }
        return $response;
    }

}