<?php


namespace App\Http\Controllers;


use App\Libraries\Solr;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    function index(Request $request)
    {
        $q = $request->get('q');
        if($q == "") {
            $query = "*:*";
        } else {
            $query = str_replace(['[', ']'], '', $q);
        }

        $start = $request->get('start') ? ($request->get('start') != "") : 0;
        $rows = $request->get('rows') ? ($request->get('rows') != "") : 1;

        $collection = 'products';
        $solr = new Solr($collection);

        $response = $solr->search($query, $start, $rows);
        if($response->ok()) {
            $result = $response->json();
            return $result['response']['docs'];
        } else {
            return response([], $response->status());
        }
    }
}